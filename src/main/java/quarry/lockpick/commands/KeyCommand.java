package quarry.lockpick.commands;

import net.kyori.adventure.text.Component;
import net.kyori.adventure.text.format.TextDecoration;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import quarry.lockpick.Lockpick;
import quarry.lockpick.utils.BlockUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;


public class KeyCommand implements CommandExecutor, TabCompleter {

    private final Lockpick plugin;

    public KeyCommand(Lockpick plugin) {
        this.plugin = plugin;
    }

    @Override
    public boolean onCommand(@NotNull CommandSender sender, @NotNull Command command, @NotNull String label, @NotNull String[] args) {
        if(sender instanceof Player player) {
            if (args[0].equalsIgnoreCase("get")) {
                UUID uuid = UUID.randomUUID();
                ItemStack stack = new ItemStack(Material.TRIPWIRE_HOOK);
                ItemMeta meta = stack.getItemMeta();

                BlockUtils.setPersistantKey(plugin, "keyid", uuid.toString(), meta);
                meta.displayName(Component.text("Chest Key").decorate(TextDecoration.BOLD));
                List<Component> temp = new ArrayList<>();
                temp.add(Component.text(uuid.toString()));
                meta.lore(temp);

                stack.setItemMeta(meta);

                player.getInventory().setItemInMainHand(stack);
            }
        }

        return false;
    }

    @Override
    public @Nullable List<String> onTabComplete(@NotNull CommandSender sender, @NotNull Command command, @NotNull String alias, @NotNull String[] args) {
        if(command.getName().equalsIgnoreCase("key")){
            if(args.length == 1){
                ArrayList<String> list = new ArrayList<>();
                list.add("get");
                return list;
            }
        }

        return null;
    }
}
