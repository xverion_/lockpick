package quarry.lockpick.utils;

import org.bukkit.NamespacedKey;
import org.bukkit.block.BlockFace;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.persistence.PersistentDataType;
import quarry.lockpick.Lockpick;

public class BlockUtils {

    private static final BlockFace[] faces = new BlockFace[]{
      BlockFace.DOWN, BlockFace.UP, BlockFace.EAST, BlockFace.NORTH, BlockFace.SOUTH, BlockFace.WEST };

    public static BlockFace[] getFaces() {
        return faces;
    }

    public static boolean hasPersistantKey(Lockpick plugin, String key, ItemMeta meta){
        return meta.getPersistentDataContainer().has(new NamespacedKey(plugin, key), PersistentDataType.STRING);
    }

    public static void setPersistantKey(Lockpick plugin, String key, String uuid, ItemMeta meta){
        meta.getPersistentDataContainer().set(new NamespacedKey(plugin, key), PersistentDataType.STRING, uuid);
    }

    public static String getUUIDFromPersistantKey(Lockpick plugin, String key, ItemMeta meta){
        return meta.getPersistentDataContainer().get(new NamespacedKey(plugin, key), PersistentDataType.STRING);
    }
}
