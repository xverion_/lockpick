package quarry.lockpick.database;

import quarry.lockpick.Lockpick;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class SQLiteConnection {

    private static Connection c;

    public static void setup(Lockpick plugin){
        c = null;

        try {
            Class.forName("org.sqlite.JDBC");
            c = DriverManager.getConnection("jdbc:sqlite:" + plugin.getDataFolder() + "\\lockpick.sqlite");
            //createKeyPlayerTable();

            plugin.setConnection(c);
        } catch (Exception e){
            e.printStackTrace();
        }
    }


    private static void createKeyPlayerTable(){
        Statement stmt;

        try {
            stmt = c.createStatement();
            String sql = """
                    
                    """;

            stmt.executeUpdate(sql);
            stmt.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
