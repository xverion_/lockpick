package quarry.lockpick.listeners;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import quarry.lockpick.Lockpick;
import quarry.lockpick.utils.BlockUtils;

public class ChestInteractions implements Listener {

    private final Lockpick plugin;

    public ChestInteractions(Lockpick plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void ChestOpen(InventoryOpenEvent event){
        Location loc = event.getInventory().getLocation();
        if(loc != null) {
            World world = loc.getWorld();
            Block block = world.getBlockAt(loc);

            if (block.getType().equals(Material.CHEST)) {
                System.out.println("it works");
            }
        }

    }

    @EventHandler
    public void ChestKeyClick(PlayerInteractEvent event){
        if(event.getAction() == Action.RIGHT_CLICK_BLOCK){
            if(event.getItem() != null && event.getItem().getType().equals(Material.TRIPWIRE_HOOK)){
                ItemStack item = event.getItem();

                if(BlockUtils.hasPersistantKey(plugin, "keyid", item.getItemMeta())){
                    event.setCancelled(true);
                }
            }
        }
    }
}
