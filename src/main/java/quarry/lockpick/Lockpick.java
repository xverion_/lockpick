package quarry.lockpick;

import org.bukkit.plugin.java.JavaPlugin;
import quarry.lockpick.commands.KeyCommand;
import quarry.lockpick.database.SQLiteConnection;
import quarry.lockpick.listeners.ChestInteractions;

import java.io.File;
import java.sql.Connection;
import java.util.Objects;
import java.util.logging.Logger;

public final class Lockpick extends JavaPlugin {

    public static Logger logger;

    private Connection connection;

    @Override
    public void onEnable() {
        // Plugin startup logic
        logger = getLogger();

        File configFolder = new File(getDataFolder(), "");
        configFolder.mkdir();

        //logger.info("Setting up database");

        //setupSQL();

        logger.info("Registering commands");
        Objects.requireNonNull(this.getCommand("Key")).setExecutor(new KeyCommand(getPlugin()));

        logger.info("Registering events");
        getServer().getPluginManager().registerEvents(new ChestInteractions(getPlugin()), this);
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public Lockpick getPlugin(){
        return this;
    }

    private void setupSQL() {
        SQLiteConnection.setup(getPlugin());
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
